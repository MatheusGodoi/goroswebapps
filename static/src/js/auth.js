// Author: Matheus Vinicius Gouvea de Godoi
// Tries to login, only one hardcoded user!
function FuncLogin() {
    var user = document.getElementById("inputUsername").value;
    var pass = document.getElementById("inputPassword").value;


    if((user == "123") && (pass == "123")){
        sessionStorage.setItem("logged", "true");
        alert("Logged in!")
        window.location.href = "./home.html"; 
        return;
    } else {
        alert("Failed to Log in!");
    }
};

// Checks if you are already logged in
function CheckLogged() {
    var temp = sessionStorage.getItem("logged");
    console.log("Logged? " + (temp == "true"));
    return (temp == "true");
} 

// Debug to change the logged state
function FlipLogged() {
    var temp = sessionStorage.getItem("logged");

    if(temp=="false") {
        sessionStorage.setItem("logged", "true"); 
    } else {
        sessionStorage.setItem("logged", "false"); 
    }

    document.location.reload(true);
}

// Function to register a new user
function FuncRegister(){
    //TODO: For now, it "fakes" the register and just logs you in.
    sessionStorage.setItem("logged", "true");
    alert("Registered!")
    window.location.href = "./home.html"; 
}