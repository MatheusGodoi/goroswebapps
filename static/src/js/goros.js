// Author: Matheus Vinicius Gouvea de Godoi
// Adds listeners on load, gets the bing map
window.onload = function(){
    // Load Bing maps
    map = GetMap();

    var el = document.getElementById("startlog");
    var logTicker;

    el.addEventListener("click", function() {
        // Calls the log function every 0.5 seconds
        logTicker = window.setInterval(function() {
            GetPositionLogRequest(map, logTicker);
        }, 500);
    }, false);

    el = document.getElementById("stoplog");
    el.addEventListener("click", function() {
        // Stops the timers
        window.clearInterval(logTicker) 
    }, false);
};

// Makes a Ajax GET request to fill the console with data
function GetPositionLogRequest(map, logTicker) {
    $.ajax ({
        method: "GET",
        dataType: "json",
        data: {
            topic: "/mavros/global_position/global"
        },
        url: "topic-output",
        success: function (data){
            document.getElementById("goros-console-card").innerHTML = "Latitude: " + data.Latitude + "<br>Longitude: " + data.Longitude + "<br>Altitude: " + data.Altitude;
            PushpinPositionMap(map, data.Latitude, data.Longitude)
        },
        error: function (request, status, error) {
            document.getElementById("goros-console-card").innerHTML = "Latitude: " + "ERROR" + "<br>Longitude: " + "ERROR" + "<br>Altitude: " + "ERROR";
            window.clearInterval(logTicker);
        }
    });
};

// Function to load bing map
function GetMap(){
    var map = new Microsoft.Maps.Map('#goros-map-div', {
            center: new Microsoft.Maps.Location(-22.002081, -47.932765),
            mapTypeId: Microsoft.Maps.MapTypeId.aerial,
            zoom: 18
    });

    return map
};

// Function to add a Pushpin to the map using the given coordinates
function PushpinPositionMap(map, latitude, longitude){
    // First clear all existing pushpins
    RemoveAllPushpinMap(map)
    
    // Create custom Pushpin
    var pin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(latitude, longitude), {
        icon: "./src/icons/dronepin.png",
        title: 'Position',
    });
    // Add the pushpin to the map
    map.entities.push(pin);

    // Center the map on the pin
    map.setView({
        mapTypeId: Microsoft.Maps.MapTypeId.aerial,
        center: new Microsoft.Maps.Location(latitude, longitude),
    });
};

// Function to clear ALL pushpins from the map 
function RemoveAllPushpinMap(map){
map.entities.push(Microsoft.Maps.TestDataGenerator.getPushpins(10, map.getBounds()));
    for (var i = map.entities.getLength() - 1; i >= 0; i--) {
        var pushpin = map.entities.get(i);
        if (pushpin instanceof Microsoft.Maps.Pushpin) {
            map.entities.removeAt(i);
        }
    }
};