// Author: Matheus Vinicius Gouvea de Godoi
// Hides some navbar buttons if you are logged in (or not)
window.onload = function(){
    FirstLoad()

    if(CheckLogged()){
        var loginButton = (document).getElementById("goros-navbar-login");
        var registerButton = (document).getElementById("goros-navbar-register");
        var profileButton = (document).getElementById("goros-navbar-profile");
        var gorosButton = (document).getElementById("goros-navbar-gorosbutton");

        loginButton.style.display = "none";
        registerButton.style.display = "none";
        profileButton.style.display = "block";
        gorosButton.style.display = "block";
    }
}

// Runs only once, to prevent the state from being swapped on page reloads
function FirstLoad() {
    var temp = sessionStorage.getItem("loaded");

    if (temp == "true") {
        return;
    }

    sessionStorage.setItem("loaded", "true");
    sessionStorage.setItem("logged", "false");
    loaded = true;
}