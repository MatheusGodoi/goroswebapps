module gitlab.com/MatheusGodoi/GOROSWebapp

go 1.14

require (
	github.com/aler9/goroslib v0.0.0-20200730195836-3d73c1cd6be9
	github.com/gorilla/websocket v1.4.2
)
