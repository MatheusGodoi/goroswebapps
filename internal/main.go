// Author: Matheus Vinicius Gouvea de Godoi
// Main code to render the home page
package main

import (
	"net/http"
	"log"
	"gitlab.com/MatheusGodoi/GOROSWebapp/internal/callback"
)

func main() {
	// Sets all the handlers on the code
	configHandlers()

	// Listen and Serve
	err := http.ListenAndServe(":8080", nil)
    if err != nil {
        log.Fatal("ListenAndServe: ", err)
    }
}

func configHandlers() {
	// Route handlers
	http.HandleFunc("/home", callback.HomePage)
	http.HandleFunc("/topic-output", callback.GetTopicOutput)

	// List all files on the server
	http.Handle("/", http.FileServer(http.Dir("../static/")))
}

