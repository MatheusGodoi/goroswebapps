// Author: Matheus Vinicius Gouvea de Godoi
// TODO: Package to hold generic functions
package utils

import (
	"net/http"
	"html/template"
	"math/rand"
	"time"
	// "os"
	// "io"
	// "bytes"
	// "log"
	// "os/exec"
	// "path/filepath"
)

// Generic code to render a page using a filename as template, and a interface with the information inside
func TemplateRender(w http.ResponseWriter, filename string, data interface{}) {
	template := template.Must(template.ParseFiles(filename))
    if err := template.Execute(w, data); err != nil {
        http.Error(w, err.Error(), 500)
        return
    }
}


// TODO: Remove this function
// Debug function that generates a random float number
// Unsafe for concurrent functions
func RandomFloat() float64 {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return r.Float64()
}
	


// TODO: Function to read body and get the commands, dirpath and arguments, used to run a terminal command
// Read request body for commands and pass to the runCommand function
/* func readRequestBody(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error reading body: %v", err)
		http.Error(w, "can't read body", http.StatusBadRequest)
		return
	}

	runCommand(body)
}

// TODO: Runs the command with the dirpath, command and arguments from the request body
// Runs a terminal command with directory, the command itself and the given arguments
func runCommand(dirPath string, cmdString string, args ...[]string) string{
	var argsList []string
	

	// Runs the given terminnal command

	command := exec.Command(cmdString, args...)
	command.Dir, _= filepath.Abs(dirPath)	
	
	var stdBuffer bytes.Buffer

	// Multiwriter to read the terminal output and redirect to the page
	mw := io.MultiWriter(os.Stdout, &stdBuffer)
	
	command.Stdout = mw
	command.Stderr = mw
	err := command.Run() //blocks until sub process is complete
	if err != nil {
		panic(err)
	}

	return stdBuffer.String() 
}
 */

