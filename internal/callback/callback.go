// Author: Matheus Vinicius Gouvea de Godoi
// All the callbakcs functions used in the handlers
package callback

import (
	"net/http"
	"encoding/json"
	"time"

	// "gitlab.com/MatheusGodoi/GOROSWebapp/internal/utils"

	"github.com/aler9/goroslib"
	"github.com/aler9/goroslib/msgs/sensor_msgs"
)

type ConsoleJsonStructure struct{
	Altitude float64
    Longitude float64
    Latitude float64
}

var (
	GlobalConsoleJson ConsoleJsonStructure
)

// Redirects "/home" to the homepage
func HomePage(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "./home.html", http.StatusFound)
}

// Request made with a Ajax call.
// Reads the terminal output for a given ros topic on the request body, and returns a JSON with the info
func GetTopicOutput(w http.ResponseWriter, r *http.Request){
	var (
		err error
		node *goroslib.Node
		sub *goroslib.Subscriber
		topic string
		data []byte
	)

	// Get the topic to subscribe from the request
	r.ParseForm()
	topic = r.FormValue("topic")

	if r.Method == "GET" {
		// Creates a node to subscribe
		node, err = goroslib.NewNode(goroslib.NodeConf{
			Name:       "/node-name-sub",
			MasterHost: "127.0.0.1",
		})
		if err != nil {
			panic(err)
		}

		// Switch Case to make the appropriate treatment to the data got from the topic
		if topic == "/mavros/global_position/global" {
			// Creates the subscriber
			sub, err = goroslib.NewSubscriber(goroslib.SubscriberConf{
				Node:     node,
				Topic:    topic,
				Callback: logOutputsMessage,
			})
			if err != nil {
				panic(err)
			}

			// Waits X seconds before processing the message
			time.Sleep(1 * time.Second)
			// Marshalls it
			data, err = json.MarshalIndent(GlobalConsoleJson, "", "  ")
			if err != nil {
				panic(err)
			}
		} else {
			// If no topic is found, writes a json with "nulls" on the data
			data, err := json.MarshalIndent(nil, "", "  ")
			if err != nil {
				panic(err)
			}
			w.Header().Set("Content-Type", "application/json")
			w.Write(data)	
		}
		if err != nil {
			panic(err)
		}

		// set header to 'application/json'
		w.Header().Set("Content-Type", "application/json")
		// write the response
		w.Write(data)

		// Closes the node and the subscriter
		sub.Close()
		node.Close()
	}
}

// Assigns the message values to a global structure, so we can use it outside the callback
func logOutputsMessage(msg *sensor_msgs.NavSatFix){
	GlobalConsoleJson.Altitude = msg.Altitude
	GlobalConsoleJson.Latitude = msg.Latitude
	GlobalConsoleJson.Longitude = msg.Longitude
}
