// Author: Matheus Vinicius Gouvea de Godoi
// Main code to render the home page
package main

// import (
// 	"net/http"
// 	"log"
// 	"os/exec"
// 	"html/template"
// 	"path/filepath"
// )

// func main() {
// 	// Route handlers
// 	http.HandleFunc("/home", homePage)
// 	http.HandleFunc("/goros", terminalOutputHandler)
	
// 	// List all files on the server
// 	http.Handle("/", http.FileServer(http.Dir("../static/")))
//     err := http.ListenAndServe(":8080", nil)
//     if err != nil {
//         log.Fatal("ListenAndServe: ", err)
//     }
// }

// // Redirects "/home" to the homepage
// func homePage(w http.ResponseWriter, r *http.Request) {
// 	http.Redirect(w, r, "./home.html", http.StatusFound)
// }

// // Reads the terminal output from a command and prints to the screen
// func terminalOutputHandler(w http.ResponseWriter, r *http.Request) {
	
// 	// Runs the terminal command
// 	command := exec.Command("/usr/bin/go", "run", "test.go")
// 	command.Dir, _= filepath.Abs("../../../Downloads/")

// 	out, err := command.CombinedOutput()
//     if err != nil {
//         log.Fatal(err)
//     }

// 	data :=  map[string]interface{}{"TerminalOutput": string(out)}
// 	// Gets the output and sends to the render function
// 	templateRender(w, "../static/goros.html", data)
// }

// // Generic code to render a page using a filename as template, and a interface with the information inside
// func templateRender(w http.ResponseWriter, filename string, data interface{}) {
// 	template, err := template.ParseFiles(filename)
// 	if err != nil {
//         http.Error(w, err.Error(), 500)	
// 	}
//     if err := template.Execute(w, data); err != nil {
//         http.Error(w, err.Error(), 500)
//         return
//     }
// }
