*GOROS Website related Code :tada:*

[[_TOC_]]

## Requeriments

To be able to run the code, its needed:

- ROS;
- Golang;

## Running

Clone / Download the repository, and then run `go run main.go` in `GOROSWebapp/internal` folder to start the server;
Open browser and access `localhost:8080` to get a list of files (For debug purposes), and click one of the html files;
or enter `localhost:8080/home.html` to get directly into the home page of the application.

## Implemented Features

- Basic login
> There is only a single hardcoded account, being `username: 123, password: 123`. Register is a fake screen that is not yet functional, clicking the register
button just logs into the system;
- Log screen
> A simple screen that reads ros topics messages and writes it into the front end. For now, it uses the "publisher_example.go" file to generate the message and post in a ros topic, and them start displaying them when you click "StartLog" button. Stops when you click "StopLog" button.
- Debug Utils
> There is two buttons on home page, one called "Check", that display your status (Logged in or not) and one called "Flip", that changes your login state (Logs in if
you are not already; logs out if not logged)
- Placeholders
> Some screens that are planned are created to show the planned feature (Like profile, about, goros functions...). Clicking the link will display a placeholder page.


## Planned Features

- More information
> Adding details, images, documentations on cards, pages, etc;
- Functional Login
> A database to hold users, allowing for complex authentication aswell as registering new accounts;
- Profile
> User account where you will be able to change your data (name, username, password...) and your drone hardware specs;
- Mission Control
> Full control of the drone mission, like start/stop, Arm/Disarm, Takeoff/Lan, etc;
- Drone Logs
> More information about the drone, like battery life, GPS, status, etc;
- Simulation
> Option to run a simulated mission, aswell as a real one, being able to choose between the two;
- Route Creation
> Draw your own route to be used in a mission and save it for your account;
- Mission Creation
> Define your mission to be used together with your planned routes to customize the path;